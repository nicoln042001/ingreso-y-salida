<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\People;
use App\Models\State;
use App\Models\Role;
use App\Models\Program;
	 	
class PeopleController extends Controller
{

    public function index()
    {
        $people = People::all();
        json_decode(json_encode($people), true);
        echo $people;
        return \View::make('people/list', compact('people'));
    }


    public function create()
		{
    	$roles = Role::all();
    	$states = State::all()->where('typestate_id', '=', 1);
    	$programs = Program::all();
        return \View::make('people/new', compact('roles','states','programs'));
    }


    public function store(Request $request)
    {    	
    	$people = new People;
        $people->document = $request->document;
        $people->namePerson = $request->namePerson;
        $people->state_id = $request->state_id;
        $people->role_id = $request->role_id;
        $people->program_id = $request->program_id;
        $people-> save();
    }

    public function show(Request $request)
    {
        $people = People::where('namePerson','like','%'.$request->namePerson.'%');
        $roles = Role::where('role_id','like','%'.$request->nameRole.'%');
        $states = State::where('state_id','like','%'.$request->nameState.'%');
        $programs = Program::where('program_id','like','%'.$request->name_program.'%');
        return \View::make('people/list', compact('people','roles','states','programs'));

    }

    public function edit($id)
    {
        $people = People::find($id);
        $states = State::all();
        $roles = Role::all();
        $programs = Program();
        return \View::make('people/update', compact('people', 'states','programs','roles'));
    }

    public function update(Request $request, $id)
    {
        $people = People::find($id);
        $people->document = $request->document;
        $people->namePerson = $request->namePerson;
        $people->state_id = $request->state_id;
        $people->role_id = $request->role_id;
        $people->program_id = $request->program_id;
        $people->save();
    }

    public function destroy($id)
    {
        $people = People::find($id);
        $people->delete();
         return redirect()->back();
    }
}
