<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table ='programs';
    protected $fillable = ['name_program','state_id'];
    protected $guarded = ['id'];
}
