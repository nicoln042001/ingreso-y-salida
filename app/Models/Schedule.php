<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
     protected $table ='schedules'
    protected $fillable = ['ArrivalTime','departureTime','state_id'];
    protected $guarded  =['id'];
}
