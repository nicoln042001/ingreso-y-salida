<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
     protected $table ='people';
    protected $fillable = ['document','namePerson','state_id','role_id','program_id'];
    protected $guarded = ['id'];

    public function State(){
    	return $this->belongsTo(State::class);
    }

    public function Role(){
    	return $this->belongsTo(Role::class);
    }

    public function Program(){
    	return $this->hasOne(Program::class);
    }
}
