<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Rutas de Personas//
Route::resource('people', 'PeopleController');
Route::post('people/show',['as'=> 'people/show','uses' => 'PeopleController@show']);
Route::get('people', ['as' => 'people', 'uses'=>'PeopleController@show']);
Route::get('people/destroy/{id}', ['as' => 'people/destroy', 'uses' => 'PeopleController@destroy']);
Route::put('people/{id}/update', ['as' => 'people/update', 'uses' => 'PeopleController@update']);
Route::post('people/store', ['as' => 'people/store', 'uses'=>'PeopleController@store']);


