<?php

use Illuminate\Database\Seeder;
use App\Models\TypeState;
class TypeStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
          $typestate = array(
        	[
        		'typestate'=> 'Usuarios'
        		       	
        	],
        	[
        		'typestate'=> 'Novedades'
        		       	
        	]);
			
        foreach ($typestate as $value) {
         	$typestate = new TypeState;
         	$typestate->typestate = $value['typestate'];
            $typestate->save();
    	}
    }
}
