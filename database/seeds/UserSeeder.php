<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = array(
         	[
         		'name' => 'Nicol',
         		'email' => 'nicoln042001@gmail.com',
         		'password' => '123456789',
         		'people_id'=> 1,
         		'state_id' => '1',

			]);
        foreach ($user as $value) {
         	$user = new User;
         	$user->name = $value['name'];
         	$user->email = $value['email'];
         	$user->password = Hash::make($value['password']);
         	$user->people_id = $value['people_id'];
         	$user->state_id = $value['state_id'];
            $user->save();
    	}
    }
}
