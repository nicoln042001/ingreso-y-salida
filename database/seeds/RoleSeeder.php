<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = array (
        	[
        		'nameRole' => 'Coordinador'
        	],
        	[
        		'nameRole' => 'Administrador'
        	]);
        foreach ($role as $value) {
         	$role = new Role;
         	$role->nameRole = $value['nameRole'];
            $role->save();
	    }

    }
}
