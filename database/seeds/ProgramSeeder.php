<?php

use Illuminate\Database\Seeder;
use App\Models\Program;
class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $program = array(
        	[
        		'name_program' => 'ambiental',
        		'state_id' => '1',
        	]);
        foreach ($program as $value) {
         	$program = new Program;
         	$program->name_program = $value['name_program'];
         	$program->state_id = $value['state_id'];
            $program->save();
	    }
    }
}
