<?php

use Illuminate\Database\Seeder;
use App\Models\People;
class PeopleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $person = array(
        	[
        		'document' => '1001093007',
        		'namePerson' =>'Nicol Ramirez',
        		'state_id' => 1,
        		'role_id' => 1,
        		'program_id' => 1, 
        	]);
        foreach ($person as $value) {
         	$person = new People;
         	$person->document = $value['document'];
         	$person->namePerson = $value['namePerson'];
         	$person->state_id = $value['state_id'];
         	$person->role_id = $value['role_id'];
         	$person->program_id = $value['program_id'];
            $person->save();
	    }
	}
}

