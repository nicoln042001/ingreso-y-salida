<?php

use Illuminate\Database\Seeder;
use App\Models\State;
class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state = array(
        	[
        		'nameState'=> 'Activo',
        		'typestate_id'=> 1,
        	],
        	[
        		'nameState'=> 'Inactivo',
        		'typestate_id'=> 1,
        	],
        	[
        		'nameState' =>'Entrada',
        		'typestate_id'=> 2
        	],
    		[
    			'nameState' => 'Salida',
    			'typestate_id' => 2
    		]
    	);
         foreach ($state as $value) {
         	$state = new State;
         	$state->nameState = $value['nameState'];
         	$state->typestate_id = $value['typestate_id'];
         	$state->save();
    	}
    }
}
