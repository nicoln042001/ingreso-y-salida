<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('people', function ($table)
            {
                $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
                $table->foreign('role_id')->references('id')->on('roles')->onUpdate('cascade');
                $table->foreign('program_id')->references('id')->on('programs')->onUpdate('cascade');
            });

        Schema::table('programs', function ($table)
            {
                $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
            });

        Schema::table('schedules', function ($table)
            {
                $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
            });

        Schema::table('states', function ($table)
            {
                $table->foreign('typestate_id')->references('id')->on('typestates')->onUpdate('cascade');
            }); 

         Schema::table('users', function ($table)
            {
                $table->foreign('people_id')->references('id')->on('people')->onUpdate('cascade');
                $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
            });

         Schema::table('novelties', function ($table)
            {
                $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
                $table->foreign('schedule_id')->references('id')->on('schedules')->onUpdate('cascade');
            });
         Schema::table('novelties_people', function ($table)
            {
                $table->foreign('novelty_id')->references('id')->on('novelties')->onUpdate('cascade');
                $table->foreign('people_id')->references('id')->on('people')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
