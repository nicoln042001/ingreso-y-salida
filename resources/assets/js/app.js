
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('menu-sidebar', require('./components/menu-sidebar.vue'));
Vue.component('listpeople', require('./components/listPeople.vue'));
Vue.component('people', require('./components/newPeople.vue'));
Vue.component('initial', require('./components/initialList.vue'));

const app = new Vue({
    el: '#app'
});
