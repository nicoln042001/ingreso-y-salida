@extends('layouts.app')
@section('content')
<head>
     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<div class="container content">
    	<section class="container"><h1 class="title">INGRESO Y SALIDA DE DOCENTES</h1>
        </section>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-9 col-md-7 col-lg-6 ">
               <div class="account-wall box-content login-box-height space form-group form-control-login" id="login">
                      <div class="justify-content-center">
                         <h1 class=" title-login m-b-md">Iniciar Sesión</h1>
                      </div>
                      <hr>
                        <form class="form-signin " method="POST" action="{{url ('/login') }}" aria-label="{{ __('Login') }}">
                            {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class=" col-form-label text-md-right">{{ __('E-mail') }}</label>
                                     <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Usuario" value="{{ old('email') }}" required autofocus>
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                         @endif
                                    </div>
                                </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="document" class=" col-form-label text-md-right">{{ __('Contraseña') }}</label>
                                <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                   </span>
                                @endif
                            </div>
                            <div class="form-group row">
                                <div class="offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember {{ old('remember') ? 'checked' : '' }}">

                                        <label class="form-check-label" for="remember">
                                            {{ __('Recordar mis Datos') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                    <button type="submit" class="col-lg-4 btn-login btn btn-primary">
                                        {{ __('Ingresar') }}
                                    </button>

                                <div class=" ">
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('¿Olvidaste tu contraseña?') }}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
@endsection
