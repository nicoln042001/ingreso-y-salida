<!--<!doctype html>-->
@extends('layouts.app')
@section('content')
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>UMB</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!--Styles-->

               <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!--    <style>
            .content{
                background-color: #fff;
                height: 50%;
            }
        </style>-->
    </head>
    <body> 
    	<section class="container"><h1 class="title">INGRESO Y SALIDA DE DOCENTES</h1></section>
        <section class="seccion">
                <div class="container-1">
                    <label class="lbl">Ingrese su Cédula</label>
                    <input type="number" name="document" class="form-control" id="frm">
                    <button class="btn btn-primary" id="boton">Enviar</button>        
                </div>
        </section>
    </body>
</html>
@endsection
 